library(igraph)

#Create graph
g <- make_empty_graph(n=0, directed = TRUE) +
  
  #Set nodes
  vertices("sLP1", "sLP2", "sMP", "sHP1", "sHP2", "sLK1", "sLK2", "sMK",
           "sHK", "fHP", "cLP", "cMP", "cHP", "cLK", "cMK", "cHK",
           "jLP", "jMP", "jHP", "jLK", "jMK", "jHK", "qcLP", "qcHP", "qcLK",
           "qcMK", "qcHK", "jqcLK", "jqcMK", "jqcHK") +
  
  #Adding all edges needed. Edges represent what moves have enough hitstun
  #to be able to move in to another move. Edges will be weighted using
  #the move's undizzy value, which when the sum reaches 240+ allows the
  #opponent to break out of the combo
  
  #Standing normals
  edge("sLP1","sLP2",  weight = 15) + edge("sLP2","sMP",  weight = 20) +
  edge("sMP","sHP1",  weight = 30) + edge("sHP1","sHP2",  weight = 30) +
  edge("sLK1","sLK2",  weight = 15) + edge("sLK2","sMK",  weight = 20) +
  edge("sMK","sHK",  weight = 30) + edge("sLP2","sMK",  weight = 20) +
  edge("sMK","sHP1",  weight = 30) + edge("sHP1","sHP2",  weight = 30) +
  edge("sLK2","sMP",  weight = 20) + edge("sMP","sHK",  weight = 20) + 
  edge("sMP","fHP",  weight = 30) + edge("sMK","fHP",  weight = 30) +
  
  #Crouching normals
  edge("cLP","cMP",  weight = 20) + edge("cMP","cHP",  weight = 30) +
  edge("cLK","cMK",  weight = 20) + edge("cMK","cHK",  weight = 30) +
  edge("cLP","cMK",  weight = 20) + edge("cLK","cMP",  weight = 20) +
  edge("cMP","cHK",  weight = 30) + edge("cMK","cHP",  weight = 30) +
  
  #Standing chaining into crouching
  edge("sLP2","cMP",  weight = 20) + edge("sLK2","cMK",  weight = 20) +
  edge("sLP2","cMK",  weight = 20) + edge("sLK2","cMP",  weight = 20) +
  edge("cLP","sMP",  weight = 20) + edge("cLK","sMK",  weight = 20) +
  edge("cLP","sMK",  weight = 20) + edge("cLK","sMP",  weight = 20) +
  edge("sMP","cHP",  weight = 30) + edge("sMK","cHK",  weight = 30) +
  edge("sMP","cHK",  weight = 30) + edge("sMK","cHP",  weight = 30) +
  edge("cMP","sHP1",  weight = 30) + edge("cMK","sHK",  weight = 30) +
  edge("cMP","sHK",  weight = 30) + edge("cMK","sHP1",  weight = 30) +
  
  #Jumping normals
  edge("sHK","jLP",  weight = 15) + edge("jLP","jMP",  weight = 20) +
  edge("jMP","jHP",  weight = 30) + edge("sHK","jLK",  weight = 15) +
  edge("jLK","jMK",  weight = 20) + edge("jMK","jHK",  weight = 30) +
  edge("jLP","jMK",  weight = 20) + edge("jLK","jMP",  weight = 20) +
  edge("jMP","jHK",  weight = 30) + edge("jMK","jHP",  weight = 30) +
  
  #Grounded specials
  edge("sLP2","qcLP",  weight = 20) + edge("sLP2","qcHP",  weight = 20) +
  edge("sLP2","qcLK",  weight = 20) + edge("sLP2","qcMK",  weight = 20) +
  edge("sLP2","qcHK",  weight = 20) + edge("sMP","qcLP",  weight = 20) +
  edge("sMP","qcHP",  weight = 20) + edge("sMP","qcLK",  weight = 20) +
  edge("sMP","qcMK",  weight = 20) + edge("sMP","qcHK",  weight = 20) +
  edge("sHP2","qcLP",  weight = 20) + edge("sHP2","qcHP",  weight = 20) +
  edge("sHP2","qcLK",  weight = 20) + edge("sHP2","qcMK",  weight = 20) +
  edge("sHP2","qcHK",  weight = 20) + edge("sHP2","qcLP",  weight = 20) +
  
  edge("sLK2","qcLP",  weight = 20) + edge("sLK2","qcHP",  weight = 20) +
  edge("sLK2","qcLK",  weight = 20) + edge("sLK2","qcMK",  weight = 20) +
  edge("sLK2","qcHK",  weight = 20) + edge("sMK","qcLP",  weight = 20) +
  edge("sMK","qcHP",  weight = 20) + edge("sMK","qcLK",  weight = 20) +
  edge("sMK","qcMK",  weight = 20) + edge("sMK","qcHK",  weight = 20) +
  edge("sHK","qcLP",  weight = 20) + edge("sHK","qcHP",  weight = 20) +
  edge("sHK","qcLK",  weight = 20) + edge("sHK","qcMK",  weight = 20) +
  edge("sHK","qcHK",  weight = 20) + edge("sHK","qcLP",  weight = 20) +
  
  edge("cLP","qcLP",  weight = 20) + edge("cLP","qcHP",  weight = 20) +
  edge("cLP","qcLK",  weight = 20) + edge("cLP","qcMK",  weight = 20) +
  edge("cLP","qcHK",  weight = 20) +
  edge("cLK","qcLP",  weight = 20) + edge("cLK","qcHP",  weight = 20) +
  edge("cLK","qcLK",  weight = 20) + edge("cLK","qcMK",  weight = 20) +
  edge("cLK","qcHK",  weight = 20) +
  edge("cMP","qcLP",  weight = 20) + edge("cMP","qcHP",  weight = 20) +
  edge("cMP","qcLK",  weight = 20) + edge("cMP","qcMK",  weight = 20) +
  edge("cMP","qcHK",  weight = 20) +
  edge("cMK","qcLP",  weight = 20) + edge("cMK","qcHP",  weight = 20) +
  edge("cMK","qcLK",  weight = 20) + edge("cMK","qcMK",  weight = 20) +
  edge("cMK","qcHK",  weight = 20) +
  edge("cHP","qcLP",  weight = 20) + edge("cHP","qcHP",  weight = 20) +
  edge("cHP","qcLK",  weight = 20) + edge("cHP","qcMK",  weight = 20) +
  edge("cHP","qcHK",  weight = 20) +
  
  #Special follow-ups
  edge("qcLP","sLP1",  weight = 20) + edge("qcLP","sLK1",  weight = 20) +
  edge("qcHP","sLP1",  weight = 20) + edge("qcHP","sLK1",  weight = 20) +
  edge("qcMK","sLP1",  weight = 20) + edge("jqcMK","sLK1",  weight = 20) +
  
  
  #Jumping specials
  edge("jHP","jqcLK",  weight = 20) + edge("jHK","jqcLK",  weight = 20) +
  edge("jHP","jqcMK",  weight = 20) + edge("jHK","jqcMK",  weight = 20) +
  edge("jHP","jqcHK",  weight = 20) + edge("jHK","jqcHK",  weight = 20) +
  
  #Jumping to standing
  edge("jqcMK","sLP1",  weight = 20) + edge("jqcMK","sLK1",  weight = 20) +
  edge("jqcHK","sLP1",  weight = 20) + edge("jqcHK","sLK1",  weight = 20)

#Print edges to console and print graph to plots
g
plot(g)

#Print distances with undizzy values to console
distances(g)
#Print distances with number of moves used to console
distances(g, algorithm = "unweighted")
