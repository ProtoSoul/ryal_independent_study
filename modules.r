library(hashmap)

startUpStand <- hashmap("sLP1", 6)
recoveryStand <- hashmap("sLP1", 10)
hitStunStand <- hashmap("sLP1", 20)
startUpAir <- hashmap("sLP1", 6)
recoveryAir <- hashmap("sLP1", 10)
hitStunAir <- hashmap("sLP1", 20)

startUpStand[[c("sLP2", "sMP", "sHP1", "sHP2", "sLK1", "sLK2", "sMK", "sHK", "fHP",
            "cLP", "cMP", "cHP", "cLK", "cMK", "cHK")]] <- c(8, 13, 16, 14, 8, 6, 13, 18, 22, 10, 12, 18, 8, 14, 15)

recoveryStand[[c("sLP2", "sMP", "sHP1", "sHP2", "sLK1", "sLK2", "sMK", "sHK", "fHP",
            "cLP", "cMP", "cHP", "cLK", "cMK", "cHK")]] <- c(9, 25, 24, 25, 23, 23, 24, 28, 21, 8, 18, 12, 11, 19, 26)

hitStunStand[[c("sLP2", "sMP", "sHP1", "sHP2", "sLK1", "sLK2", "sMK", "sHK", "fHP",
            "cLP", "cMP", "cHP", "cLK", "cMK", "cHK")]] <- c(20, 26, 30, 30, 20, 20, 28, 29, 24, 20, 26, 24, 20, 28, 15)

startUpAir[[c("jLP", "jMP", "jHP", "jLK", "jMK", "jHK")]] <- c(7, 10, 16, 8, 12, 15)

recoveryAir[[c("jLP", "jMP", "jHP", "jLK", "jMK", "jHK")]] <- c(16, 18, 17, 8, 16, 23)

hitStunAir[[c("jLP", "jMP", "jHP", "jLK", "jMK", "jHK")]] <- c(20, 24, 26, 24, 24, 22)